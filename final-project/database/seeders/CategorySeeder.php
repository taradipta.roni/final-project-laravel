<?php

namespace Database\Seeders;

use App\Models\Categories;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Categories::create([
        'name' => 'Kursi',
        'image' => '1687451139.jpg',
      ]);

      Categories::create([
        'name' => 'Tempat Tidur',
        'image' => '1687451203.jpg',
      ]);

      Categories::create([
        'name' => 'Aksesoris',
        'image' => '1687451283.jpg',
      ]);

      Categories::create([
        'name' => 'Meja',
        'image' => '1687451368.jpg',
      ]);

      Categories::create([
        'name' => 'Dekorasi',
        'image' => '1687451332.jpg',
      ]);
    }
}
