<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Modern Chair',
            'category_id' => 1,
            'quantity' => 100,
            'price' => 100000,
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Corporis neque iusto quos, delectus sunt eos minus est sequi ratione reprehenderit, nulla fugit nostrum a magnam, tenetur in temporibus soluta porro.',
            'image' => '1687530718.jpg'
        ]);

        Product::create([
            'name' => 'Unique Plant',
            'category_id' => 5,
            'quantity' => 100,
            'price' => 20000,
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Corporis neque iusto quos, delectus sunt eos minus est sequi ratione reprehenderit, nulla fugit nostrum a magnam, tenetur in temporibus soluta porro.',
            'image' => '1687531002.jpg'
        ]);

        Product::create([
            'name' => 'Unique Plant',
            'category_id' => 5,
            'quantity' => 100,
            'price' => 20000,
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Corporis neque iusto quos, delectus sunt eos minus est sequi ratione reprehenderit, nulla fugit nostrum a magnam, tenetur in temporibus soluta porro.',
            'image' => '1687531002.jpg'
        ]);
        Product::create([
            'name' => 'Unique Plant',
            'category_id' => 5,
            'quantity' => 100,
            'price' => 20000,
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Corporis neque iusto quos, delectus sunt eos minus est sequi ratione reprehenderit, nulla fugit nostrum a magnam, tenetur in temporibus soluta porro.',
            'image' => '1687531002.jpg'
        ]);
        Product::create([
            'name' => 'Unique Plant',
            'category_id' => 5,
            'quantity' => 100,
            'price' => 20000,
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Corporis neque iusto quos, delectus sunt eos minus est sequi ratione reprehenderit, nulla fugit nostrum a magnam, tenetur in temporibus soluta porro.',
            'image' => '1687531002.jpg'
        ]);
        Product::create([
            'name' => 'Unique Plant',
            'category_id' => 5,
            'quantity' => 100,
            'price' => 20000,
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Corporis neque iusto quos, delectus sunt eos minus est sequi ratione reprehenderit, nulla fugit nostrum a magnam, tenetur in temporibus soluta porro.',
            'image' => '1687531002.jpg'
        ]);
    }
}
