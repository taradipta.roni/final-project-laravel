<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index ()
    {
        $categories = Categories::all();
        return view('frontend.index', compact('categories'));
    }
}
