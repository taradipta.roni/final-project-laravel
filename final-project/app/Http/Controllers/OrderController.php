<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        // Mendapatkan informasi keranjang belanja
        $cart = session()->get('cart'); // Mengakses data keranjang dari sesi

        $subtotal = 0; // Inisialisasi subtotal awal

        if ($cart) {
            foreach ($cart as $item) {
                $subtotal += $item['price'] * $item['quantity']; // Menghitung subtotal setiap produk
            }
        }

        return view('frontend.checkout', compact('subtotal'));
    }

    public function store(Request $request)
    {
        // Validasi input dari pengguna (misalnya, nama, alamat, metode pembayaran, dll.)
        $request->validate([
            'payment_method' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'country' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required',
        ]);

        // Menggabungkan nilai first_name dan last_name menjadi satu nilai name
        $name = $request->input('first_name') . ' ' . $request->input('last_name');

        // Simpan data pengguna ke dalam tabel Profiles
        // $profile = Profile::create([
        //     'name' => $name,
        //     'email' => Auth::user()->email,
        //     'country' => $request->input('country'),
        //     'address' => $request->input('address'),
        //     'city' => $request->input('city'),
        //     'phone' => $request->input('phone'),
        // ]);

        // Mendapatkan data keranjang belanja (misalnya, produk, subtotal, dll.)
        $cart = session()->get('cart');

        // Menghitung total harga dari data keranjang
        $total = 0;

        if ($cart) {
            foreach ($cart as $item) {
                $total += $item['price'] * $item['quantity'];
            }
        }

        // Buat entri pesanan baru dalam tabel "orders"
        $order = Order::create([
            'user_id' => $request->user()->id, // Ganti dengan ID pengguna yang sesuai
            'order_date' => now(),
            'total' => $request->input('total'), // Ganti dengan total pesanan yang sesuai
        ]);

        // Simpan detail pesanan dalam tabel "order_details"
        foreach ($cart as $item) {
            $order->orderDetails()->create([
                'product_id' => $item['id'],
                'quantity' => $item['quantity'],
                'subtotal' => $item['price'] * $item['quantity'],
            ]);
        }

        // Buat entri pembayaran dalam tabel "payments"
        $order->payment()->create([
            'method' => $request->input('payment_method'),
            'status' => 'Pending', // Atur status pembayaran sesuai kebutuhan
        ]);

        // Kirim respon atau lakukan tindakan lain sesuai kebutuhan
        return response()->json(['message' => 'Order created successfully']);
    }
}
