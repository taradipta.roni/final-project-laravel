<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $cartItems = session()->get('cart', []);

        $subtotal = 0;

        // Menghapus duplikasi ID produk
        $uniqueProductIds = array_unique($cartItems);

        // Mengambil data produk berdasarkan ID yang ada di keranjang
        $products = [];
        foreach ($uniqueProductIds as $productId) {
            $product = Product::find($productId);
            if ($product) {
                $products[] = $product;
                // Menghitung subtotal
                $subtotal += $product->price;
            }
        }

        // dengan asumsi pengiriman free ongkir
        $total = $subtotal;

        return view('frontend.cart', compact('cartItems', 'products', 'subtotal', 'total'));
    }

    public function addToCart(Request $request)
    {
        $productId = $request->input('product_id');

        // Ambil data produk dari basis data berdasarkan ID produk
        $product = Product::find($productId);

        // Pastikan produk ditemukan
        if (!$product) {
            return response()->json(['message' => 'Product not found'], 404);
        }

        // Simpan ID produk ke dalam session cart
        $cart = session()->get('cart', []);
        $cart[] = $productId;
        session()->put('cart', $cart);

        return response()->json(['message' => 'Item added to cart successfully']);
    }

    public function delete($id)
    {
        $cartItems = session()->get('cart', []);

        // Cari item dengan ID yang sesuai dalam keranjang
        $index = array_search($id, array_column($cartItems, 'id'));

        // Jika item ditemukan, hapus item dari keranjang
        if ($index !== false) {
            unset($cartItems[$index]);
            session()->put('cart', $cartItems);
        }

        // Redirect kembali ke halaman keranjang
        return redirect('/cart');
    }
}
