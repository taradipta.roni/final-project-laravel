<?php

namespace App\Models;

use App\Models\Categories;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['name', 'image','category_id','quantity','price'];

    public function categories()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }
}
