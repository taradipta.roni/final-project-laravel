@extends('layout.dashboard')

@section('menu')
  Kategori
@endsection

@section('title')
  Buat Kategori
@endsection

@section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card mb-4">
        <div class="card-body">
          <form action="/admin/category" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
              <label class="form-label" for="name">Nama Kategori</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan nama kategori">
              @error('name')
                <p class="alert alert-danger">
                    {{ $message }}
                </p>
              @enderror
            </div>
            <div class="mb-3">
              <label for="image" class="form-label">Gambar</label>
              <input class="form-control" type="file" id="image" name="image">
            </div>
            <button type="submit" class="btn btn-primary">Tambahkan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
