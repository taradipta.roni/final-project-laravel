@extends('layout.dashboard')

@section('menu')
  Kategori  
@endsection

@section('title')
   List 
@endsection

@section('content')
<div class="card py-2">
  <div class="table-responsive text-nowrap">
    <table class="table">
      <thead>
        <tr>
          <th>No</th>
          <th style="width: 300px;">Gambar</th>
          <th>Nama Kategori</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody class="table-border-bottom-0">
        @forelse ($categories as $key => $category)
        <tr>
          <td>{{$key + 1}}</td>
          <td style="width: 300px;"><a href="/admin/category/{{$category->id}}"><img style="width: 100px;" class="img-fluid" src="{{asset('images/'. $category->image)}}" alt="image"></a></td>
          <td><a href="/admin/category/{{$category->id}}">{{$category->name}}</a></td>
          <td>
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                <i class="bx bx-dots-vertical-rounded"></i>
              </button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="/admin/category/{{$category->id}}/edit"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                <form action="/admin/category/{{$category->id}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="dropdown-item">
                    <i class="bx bx-trash me-1"></i> Delete
                  </button>
                </form>
              </div>
            </div>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="3" class="text-center">No Data Found</td>
        </tr>
        @endforelse
       
      </tbody>
    </table>
  </div>
</div>
@endsection