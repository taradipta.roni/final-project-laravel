@extends('layout.dashboard')

@section('menu')
  Kategori
@endsection

@section('title')
  Buat Kategori
@endsection

@section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card mb-4">
        <div class="card-body">
          <form action="/admin/category/{{$category->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="mb-3">
              <label class="form-label" for="name">Nama Kategori</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan nama kategori" value="{{$category->name}}">
              @error('name')
                <p class="alert alert-danger">
                    {{ $message }}
                </p>
              @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
