@extends('layout.dashboard')

@section('menu')
  Kategori
@endsection

@section('title')
  {{$category->name}}
@endsection

@section('content')
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        {{$category->name}}
      </div>
    </div>
  </div>
@endsection