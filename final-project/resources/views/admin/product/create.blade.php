@extends('layout.dashboard')

@push('scripts')
<script src="https://cdn.tiny.cloud/1/swyppviw5nqhq4h1ifu8kehzq16vtbqp98svjoofs3l7kl4x/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
@endpush

@section('menu')
Kategori
@endsection

@section('title')
Buat Kategori
@endsection

@section('content')
<div class="card mb-4">
    <div class="card-body">
        <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
            <div class="row">
                @csrf
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label" for="name">Nama Barang</label>
                        <input type="text" class="form-control" id="name" name="name"
                            placeholder="Masukkan nama kategori">
                        @error('name')
                        <p class="alert alert-danger">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="categories">Kategori</label>
                        <select id="defaultSelect" name="categories" class="form-select">
                            <option value="">Masukkan Kategori</option>
                            @foreach ($categories as $data)
                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                            @endforeach
                        </select>
                        @error('categories')
                        <p class="alert alert-danger">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Gambar</label>
                        <input class="form-control" type="file" id="image" name="image">
                        @error('image')
                        <p class="alert alert-danger">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="Price">Harga</label>
                        <div class="input-group input-group-merge">
                            <span class="input-group-text">Rp</span>
                            <input type="number" min="1000" class="form-control" name="price" placeholder="100" aria-label="Amount (to the nearest dollar)">
                            <span class="input-group-text">.00</span>
                        </div>
                        @error('PRIC')
                        <p class="alert alert-danger">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambahkan</button>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label" for="Price">Stock</label>
                        <input type="number" min="1" name="quantity" class="form-control" name="price" placeholder="100">
                        @error('price')
                        <p class="alert alert-danger">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <div>
                        <label for="exampleFormControlTextarea1" class="form-label">Deskripsi</label>
                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="8"></textarea>
                        @error('description')
                        <p class="alert alert-danger">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endsection

@push('tinyMCE')
<script>
  tinymce.init({
    selector: '#exampleFormControlTextarea1',
    height: 300,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | ' +
      'bold italic backcolor | alignleft aligncenter ' +
      'alignright alignjustify | bullist numlist outdent indent | ' +
      'removeformat | help',
  });
</script>
@endpush

@push('select2')
<script>
    $(document).ready(function() {
        $('#defaultSelect').select2();
    });
</script>
@endpush
