@extends('layout.dashboard')

@section('menu')
  Produk
@endsection

@section('title')
   List 
@endsection

@section('content')
<div class="card py-2">
  <div class="table-responsive text-nowrap">
    <table class="table">
      <thead>
        <tr>
          <th>No</th>
          <th class="text-center">Gambar</th>
          <th>Nama Barang</th>
          <th>Harga</th>
          <th>Jumlah</th>
          <th>Deskripsi</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody class="table-border-bottom-0" style="height: 150px">
        @forelse ($products as $key => $product)
        <tr>
          <td>{{$key + 1}}</td>
          <td style="width: 125px;"><a href="{{ Route('product.show', $product->id) }}"><img style="width: 100px;" class="img-fluid" src="{{asset('images/'. $product->image)}}" alt="image"></a></td>
          <td><a class="text-black" href="product/{{$product->id}}">{{$product->name}}</a></td>
          <td><a class="text-black" href="product/{{$product->id}}">{{number_format($product->price, 2, '.', ',')}}</a></td>
          <td><a class="text-black" href="product/{{$product->id}}">{{$product->quantity}}</a></td>
          <td><a class="text-black" href="product/{{$product->id}}">{{ Str::limit(strip_tags($product->description), 10, '...')}}</a></td>
          <td>
            <div class="dropdown">
              <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                <i class="bx bx-dots-vertical-rounded"></i>
              </button>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{Route('product.edit',$product->id)}}"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                <form action="{{Route('product.destroy',$product->id)}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="dropdown-item">
                    <i class="bx bx-trash me-1"></i> Delete
                  </button>
                </form>
              </div>
            </div>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="7" class="text-center">No Data Found</td>
        </tr>
        @endforelse
       
      </tbody>
    </table>
  </div>
</div>
@endsection