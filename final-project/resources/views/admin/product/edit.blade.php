@extends('layout.dashboard')

@section('menu')
Kategori
@endsection

@section('title')
Buat Kategori
@endsection

@section('content')
<div class="card mb-4">
    <div class="card-body">
        <form action="{{ route('product.update',$products->id) }}" method="POST" enctype="multipart/form-data">
            <div class="row">
                @csrf
                @method('PUT')
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label" for="name">Nama Barang</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$products->name}} "
                            >
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="categories">Kategori</label>
                        <input type="text" class="form-control" value="{{$categories->name}}" >
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="name">Harga</label>
                        <input type="text" class="form-control" id="name" name="price" value="{{$products->price}}" >
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Gambar</label>
                        <input class="form-control" value="{{asset('images/'. $products->image)}}" type="file" id="image" name="image">
                        <img style="width: 100px;" class="img-fluid" src="{{asset('images/'. $products->image)}}" alt="image">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
                <div class="col-md-6">
                  <div class="mb-3">
                    <label class="form-label" for="name">Stock</label>
                    <input type="text" name="quantity" class="form-control" id="name" name="name"
                        value="{{number_format($products->quantity, 0, '', '.')}}" >
                </div>
                    <div>
                        <label for="exampleFormControlTextarea1" class="form-label">Deskripsi</label>
                        <textarea class="form-control" name="description" name="description" id="exampleFormControlTextarea1" rows="11" >{{ $products->description }}</textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endsection
