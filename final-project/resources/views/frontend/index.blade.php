@extends('layout.frontend')

@section('content')
  <!-- Product Catagories Area Start -->
    <div class="products-catagories-area clearfix">
        <div class="amado-pro-catagory clearfix">

            <!-- Single Catagory -->
            @foreach ($categories as $category)
            <div class="single-products-catagory clearfix">
              <a href="/shop/{{$category->id}}">
                <img src="{{asset('images/' . $category->image)}}" alt="category">
                <!-- Hover Content -->
                <div class="hover-content">
                    <div class="line"></div>
                    <h4>{{$category->name}}</h4>
                </div>
              </a>
            </div>

            @endforeach
        </div>
    </div>
    <!-- Product Catagories Area End -->
@endsection