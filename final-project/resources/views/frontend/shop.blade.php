@extends('layout.frontend')

@push('csrf-token')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
<div class="shop_sidebar_area">
    <!-- ##### Single Widget ##### -->
    <div class="widget catagory mb-50">
        <!-- Widget Title -->
        <h6 class="widget-title mb-30">Categories</h6>

        <!--  Catagories  -->
        <div class="catagories-menu">
            <ul>
                <li class="{{ Request::is('shop') ? 'active' : '' }}"><a href="/shop">Semua Kategori</a></li>
              @foreach($categories as $category)
                <li class="{{ Request::segment(2) == $category->id ? 'active' : '' }}"><a href="{{url('shop/'. $category->id)}}">{{$category->name}}</a></li>
              @endforeach 
            </ul>
        </div>
    </div>

    <!-- ##### Single Widget ##### -->
    <div class="widget price mb-50">
        <!-- Widget Title -->
        <h6 class="widget-title mb-30">Price</h6>

        <div class="widget-desc">
            <div class="slider-range">
                <div data-min="10" data-max="1000" data-unit="$" class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" data-value-min="10" data-value-max="1000" data-label-result="">
                    <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>
                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span>
                <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div></div>
                <div class="range-price">$10 - $1000</div>
            </div>
        </div>
    </div>
</div>

{{-- content products --}}

<div class="amado_product_area section-padding-100">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="product-topbar d-xl-flex align-items-end justify-content-between">
                            <!-- Total Products -->
                            <div class="total-products">
                                <p>Showing 1-8 0f 25</p>
                                <div class="view d-flex">
                                    <a href="#"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <!-- Sorting -->
                            <div class="product-sorting d-flex">
                                <div class="sort-by-date d-flex align-items-center mr-15">
                                    <p>Sort by</p>
                                    <form action="#" method="get">
                                        <select name="select" id="sortBydate" style="display: none;">
                                            <option value="value">Date</option>
                                            <option value="value">Newest</option>
                                            <option value="value">Popular</option>
                                        </select><div class="nice-select" tabindex="0"><span class="current">Date</span><ul class="list"><li data-value="value" class="option selected">Date</li><li data-value="value" class="option">Newest</li><li data-value="value" class="option">Popular</li></ul></div>
                                    </form>
                                </div>
                                <div class="view-product d-flex align-items-center">
                                    <p>View</p>
                                    <form action="#" method="get">
                                        <select name="select" id="viewProduct" style="display: none;">
                                            <option value="value">12</option>
                                            <option value="value">24</option>
                                            <option value="value">48</option>
                                            <option value="value">96</option>
                                        </select><div class="nice-select" tabindex="0"><span class="current">12</span><ul class="list"><li data-value="value" class="option selected">12</li><li data-value="value" class="option">24</li><li data-value="value" class="option">48</li><li data-value="value" class="option">96</li></ul></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @foreach ($products as $product)
                        <!-- Single Product Area -->
                    <div class="col-12 col-sm-6 col-md-12 col-xl-6">
                        <div class="single-product-wrapper">
                            <!-- Product Image -->
                            <a href="/product/{{$product->id}}" class="product-img d-block">
                                <img src="{{asset('images/' . $product->image)}}" alt="" style="max-height: 500px; object-fit: cover;">
                                <!-- Hover Thumb -->
                                <img class="hover-img" src="{{asset('images/' . $product->image)}}" alt="">
                            </a>

                            <!-- Product Description -->
                            <div class="product-description d-flex align-items-center justify-content-between">
                                <!-- Product Meta Data -->
                                <div class="product-meta-data">
                                    <div class="line"></div>
                                    <p class="product-price">{{$product->price}}</p>
                                    <a href="product-details.html">
                                        <h6>{{$product->name}}</h6>
                                    </a>
                                </div>
                                <!-- Ratings & Cart -->
                                <div class="ratings-cart text-right">
                                    <div class="ratings">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <div class="cart">
                                        <a href="#" class="add-to-cart" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add to Cart"><img src="{{ asset('assets/img/core-img/cart.png') }}" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    

                </div>

                <div class="row">
                    <div class="col-12">
                        <!-- Pagination -->
                        <nav aria-label="navigation">
                            <ul class="pagination justify-content-end mt-50">
                                <li class="page-item active"><a class="page-link" href="#">01.</a></li>
                                <li class="page-item"><a class="page-link" href="#">02.</a></li>
                                <li class="page-item"><a class="page-link" href="#">03.</a></li>
                                <li class="page-item"><a class="page-link" href="#">04.</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('addtocart-script')
<script>
     $(document).ready(function() {
        // Setel token CSRF pada setiap permintaan AJAX
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Tangkap event klik tombol "Add to Cart"
        $('.add-to-cart').click(function(e) {
            e.preventDefault();

            // Ambil ID produk dari atribut data
            var productId = $(this).data('product');

            // Panggil fungsi addToCart dan berikan ID produk sebagai parameter
            addToCart(productId);
        });

        function addToCart(productId) {
            // Kirim permintaan AJAX ke route addToCart
            $.ajax({
                url: '/add-to-cart',
                method: 'POST',
                data: { product_id: productId },
                success: function(response) {
                    // Tampilkan pesan sukses menggunakan SweetAlert2
                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                },
                error: function(xhr, status, error) {
                    // Tampilkan pesan error jika terjadi kesalahan
                    alert('Terjadi kesalahan. Silakan coba lagi.');
                }
            });
        }
    });
</script>
@endpush