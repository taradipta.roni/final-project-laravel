@extends('layout.frontend')

@section('content')
  <div class="cart-table-area section-padding-100">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 col-lg-8">
        <div class="checkout_details_area mt-50 clearfix">
          <div class="cart-title">
              <h2>Checkout</h2>
          </div>
          <form action="/checkout" method="post">
          @csrf
            <div class="row">
              <div class="col-md-6 mb-3">
                <input type="text" class="form-control" id="first_name" value="" placeholder="First Name" required="" name="first_name">
              </div>
              <div class="col-md-6 mb-3">
                <input type="text" class="form-control" id="last_name" value="" placeholder="Last Name" required="" name="last_name">
              </div>
              <div class="col-12 mb-3">
                <input type="email" class="form-control" id="email" placeholder="Email" value="" name="email">
              </div>
              <div class="col-12 mb-3">
                <select class="w-100" id="country" style="display: none;" name="country">
                  <option value="usa">United States</option>
                  <option value="uk">United Kingdom</option>
                  <option value="ger">Germany</option>
                  <option value="fra">France</option>
                  <option value="ind">India</option>
                  <option value="aus">Australia</option>
                  <option value="bra">Brazil</option>
                  <option value="cana">Canada</option>
                </select>
                <div class="nice-select w-100" tabindex="0">
                  <span class="current">United States</span>
                  <ul class="list">
                    <li data-value="usa" class="option selected focus">United States</li>
                    <li data-value="uk" class="option">United Kingdom</li>
                    <li data-value="ger" class="option">Germany</li>
                    <li data-value="fra" class="option">France</li>
                    <li data-value="ind" class="option">India</li>
                    <li data-value="aus" class="option">Australia</li>
                    <li data-value="bra" class="option">Brazil</li>
                    <li data-value="cana" class="option">Canada</li>
                  </ul>
                </div>
              </div>
              <div class="col-12 mb-3">
                  <input type="text" class="form-control mb-3" id="street_address" placeholder="Address" value="" name="address">
              </div>
              <div class="col-12 mb-3">
                  <input type="text" class="form-control" id="city" placeholder="Town" value="" name="city">
              </div>
              <div class="col-md-12 mb-3">
                  <input type="number" class="form-control" id="phone" min="0" placeholder="Phone No" value="" name="phone">
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-12 col-lg-4">
        <div class="cart-summary">
          <h5>Cart Total</h5>
          <ul class="summary-table">
              <li><span>subtotal:</span> <span>{{$subtotal}}</span></li>
              <li><span>delivery:</span> <span>Free</span></li>
              <li><span>total:</span> <span>{{$subtotal}}</span></li>
          </ul>

          <div class="payment-method">
            <!-- Cash on delivery -->
            <div class="custom-control custom-radio mr-sm-2">
                <input type="radio" class="custom-control-input" id="cod" checked="" name="payment_method">
                <label class="custom-control-label" for="cod">Cash on Delivery</label>
            </div>
            <!-- Paypal -->
            <div class="custom-control custom-radio mr-sm-2">
                <input type="radio" class="custom-control-input" id="paypal" name="payment_method">
                <label class="custom-control-label" for="paypal">Paypal<img class="ml-15" src="{{asset('/assets/img/core-img/paypal.png')}}" alt=""></label>
            </div>
          </div>

          <div class="cart-btn mt-100">
            <button type="submit" class="btn amado-btn w-100">Checkout</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  
@endsection
