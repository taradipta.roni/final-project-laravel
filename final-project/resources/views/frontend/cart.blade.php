@extends('layout.frontend')

@section('content')
  <div class="cart-table-area section-padding-100">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-lg-8">
          <div class="cart-title mt-50">
            <h2>Shopping Cart</h2>
          </div>
          @if (count($cartItems) > 0)
          <div class="cart-table clearfix">
            <table class="table table-responsive" tabindex="1" style="overflow: hidden; outline: none;">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($products as $key => $product)
                <tr>
                  <td class="cart_product_img">
                    <a href="#"><img src="{{asset('images/'. $product->image)}}" alt="Product"></a>
                  </td>
                  <td class="cart_product_desc">
                    <h5>{{ $product->name }}</h5>
                  </td>
                  <td class="price">
                    <span>{{ $product->price }}</span>
                  </td>
                  <td class="qty d-flex">
                    <div class="qty-btn d-flex">
                      <p>Qty</p>
                      <div class="quantity">
                        <span class="qty-minus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty > 1 ) effect.value--;return false;">
                          <i class="fa fa-minus" aria-hidden="true"></i>
                        </span>
                        <input type="number" id="qty{{ $key }}" class="qty-text" id="qty" step="1" min="1" max="300" name="quantity" value="{{ count(array_keys($cartItems, $product->id)) }}">
                        <span class="qty-plus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;">
                          <i class="fa fa-plus" aria-hidden="true"></i>
                        </span>
                      </div>
                    </div>
                    <form action="/delete-cart/{{$product->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger ml-2">delete</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          <p>Tidak ada produk di keranjang</p>
          @endif
        </div>
        <div class="col-12 col-lg-4">
          <div class="cart-summary">
            <h5>Cart Total</h5>
            <ul class="summary-table">
                <li><span>subtotal:</span> <span>{{$subtotal}}</span></li>
                <li><span>delivery:</span> <span>Free</span></li>
                <li><span>total:</span> <span>{{$total}}</span></li>
            </ul>
            <div class="cart-btn mt-100">
              <a href="/checkout" class="btn amado-btn w-100">Checkout</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection