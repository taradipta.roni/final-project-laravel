<ul class="menu-inner py-1">
  <!-- Dashboard -->
  <li class="menu-item {{ Request::is('admin') ? 'active' : '' }}">
    <a href="/admin" class="menu-link">
      <i class="menu-icon tf-icons bx bx-home-circle"></i>
      <div data-i18n="Analytics">Dashboard</div>
    </a>
  </li>

  <!-- Layouts -->
  <li class="menu-header small text-uppercase">
    <span class="menu-header-text">Products</span>
  </li>
  <li class="menu-item {{ Request::is('admin/category*') ? 'active open' : '' }}">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
      <i class="menu-icon tf-icons bx bx-layout"></i>
      <div data-i18n="Layouts">Categories</div>
    </a>

    <ul class="menu-sub">
      <li class="menu-item {{ Request::is('admin/category/create') ? 'active' : '' }}">
        <a href="/admin/category/create" class="menu-link">
          <div data-i18n="Without menu">Add Category</div>
        </a>
      </li>
      <li class="menu-item {{ Request::is('admin/category') ? 'active' : '' }}">
        <a href="/admin/category" class="menu-link">
          <div data-i18n="Without navbar">List Categories</div>
        </a>
      </li>
    </ul>
  </li>
  <li class="menu-item {{ Request::is('admin/product*') ? 'active open' : '' }}">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
      <i class="menu-icon tf-icons bx bx-layout"></i>
      <div data-i18n="Layouts">Products</div>
    </a>

    <ul class="menu-sub">
      <li class="menu-item {{ Request::is('admin/product/create') ? 'active' : '' }}">
        <a href="{{ route('product.create') }}" class="menu-link">
          <div data-i18n="Without menu">Add Product</div>
        </a>
      </li>
      <li class="menu-item {{ Request::is('admin/product') ? 'active' : '' }}">
        <a href="{{ route('product.index') }}" class="menu-link">
          <div data-i18n="Without navbar">List Products</div>
        </a>
      </li>
    </ul>
  </li>

   <!-- Transaction -->
  <li class="menu-header small text-uppercase">
    <span class="menu-header-text">Transactions</span>
  </li>
  

  <!-- Misc -->
  <li class="menu-header small text-uppercase"><span class="menu-header-text">Users</span></li>
  <li class="menu-item">
    <a
      href="https://github.com/themeselection/sneat-html-admin-template-free/issues"
      target="_blank"
      class="menu-link"
    >
      <i class="menu-icon tf-icons bx bx-support"></i>
      <div data-i18n="Support">Users</div>
    </a>
  </li>
  <li class="menu-item">
    <a
      href="https://themeselection.com/demo/sneat-bootstrap-html-admin-template/documentation/"
      target="_blank"
      class="menu-link"
    >
      <i class="menu-icon tf-icons bx bx-file"></i>
      <div data-i18n="Documentation">Documentation</div>
    </a>
  </li>
</ul>