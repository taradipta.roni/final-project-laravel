<!-- Header Area Start -->
<header class="header-area clearfix">
  <!-- Close Icon -->
  <div class="nav-close">
      <i class="fa fa-close" aria-hidden="true"></i>
  </div>
  <!-- Logo -->
  <div class="logo">
      <a href="index.html"><img src="{{asset('/assets/img/core-img/logo.png')}}" alt=""></a>
  </div>
  <!-- Amado Nav -->
  <nav class="amado-nav">
    <ul>
        <li class="{{ Request::is('/') ? 'active' : '' }}">
            <a href="/">Home</a>
        </li>
        <li class="{{ Request::is('shop*') ? 'active' : '' }}">
            <a href="/shop">Shop</a>
        </li>
        <li class="{{ Request::is('cart*') ? 'active' : '' }}">
            <a href="/cart">Cart</a>
        </li>
        <li class="{{ Request::is('checkout*') ? 'active' : '' }}">
            <a href="/checkout">Checkout</a>
        </li>
    </ul>
  </nav>
  <!-- Button Group -->
  <div class="amado-btn-group mt-30 mb-100">
      <a href="#" class="btn amado-btn mb-15">%Discount%</a>
      <a href="#" class="btn amado-btn active">New this week</a>
  </div>
  <!-- Cart Menu -->
  <div class="cart-fav-search mb-100">
    <a href="/cart" class="cart-nav"><img src="{{asset('/assets/img/core-img/cart.png')}}" alt=""> Cart <span>({{ count(session('cart', [])) }})</span></a>
    <a href="#" class="search-nav"><img src="{{asset('/assets/img/core-img/search.png')}}" alt=""> Search</a>
    @guest
        <a href="/login" class="fav-nav bg-warning text-center">Login</a>
    @endguest
    @auth
    <form action="{{ route('logout') }}" method="POST" class="fav-nav bg-warning text-center">
        @csrf
      <button class="btn btn-warning w-100" type="submit">{{ __('Logout') }}</button>
    </form>
    @endauth

  </div>
  <!-- Social Button -->
  <div class="social-info d-flex justify-content-between">
    <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
  </div>
</header>
<!-- Header Area End -->